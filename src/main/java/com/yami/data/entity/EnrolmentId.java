package com.yami.data.entity;
// Generated 22/09/2017 10:19:54 AM by Hibernate Tools 5.2.5.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * EnrolmentId generated by hbm2java
 */
@Embeddable
public class EnrolmentId implements java.io.Serializable {

    private String classCode;
    private int stuId;

    public EnrolmentId() {
    }

    public EnrolmentId(String classCode, int stuId) {
        this.classCode = classCode;
        this.stuId = stuId;
    }

    @Column(name = "CLASS_CODE", nullable = false, length = 5)
    public String getClassCode() {
        return this.classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    @Column(name = "STU_ID", nullable = false)
    public int getStuId() {
        return this.stuId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof EnrolmentId))
            return false;
        EnrolmentId castOther = (EnrolmentId) other;

        return ((this.getClassCode() == castOther.getClassCode()) || (this.getClassCode() != null && castOther.getClassCode() != null && this.getClassCode().equals(castOther.getClassCode()))) && (this.getStuId() == castOther.getStuId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getClassCode() == null ? 0 : this.getClassCode().hashCode());
        result = 37 * result + this.getStuId();
        return result;
    }

}
